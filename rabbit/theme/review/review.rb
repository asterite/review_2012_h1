include_theme("rabbit")

@image_timer_limit = 10 * 60 # in seconds
include_theme("image-timer")

@slide_number_uninstall = true
include_theme("slide-number")

include_theme("image-slide-number")