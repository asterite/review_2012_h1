= Review Semestral

: subtitle
   Qué hace un programador?
: author
   Ary Borenszweig
: institution
   Manas
: theme
   review

= ¿Qué hace un programador?

  * (('wait'))Ve problemas
  * (('wait'))Encuentra soluciones
  * (('wait'))Simplifica
  * (('wait'))Automatiza

= ¿Qué hace Ary?

= ¿Qué hace Ary?

  # image
  # src = img/challenge.jpg
  # keep_ratio = true
  # width = 800
  # height = 600 

= De-Bee

  * (('wait'))El problema de juntar plata
  * (('wait'))Y repartir el vuelto

= De-Bee

  * ¿Ganancia para Manas?
  * (('wait'))$0
  * (('wait')):-(

= Citatum

  * (('wait'))Engorroso modificar la wiki
  * (('wait'))Nadie se entera

= Citatum

  # blockquote
  # title = Ary (review 2012-H2)
  Ésta es mi frase favorita

= Citatum

  * ¿Ganancia para Manas?
  * (('wait'))$0
  * (('wait')):-(

= Brium

  * (('wait'))Engorroso el seguimiento de proyectos
  * (('wait'))Difícil de contablizar horas
  * (('wait'))En tiempo real

= Brium

  * ¿Ganancia para Manas?
  * (('wait'))$0
  * (('wait'))Por ahora...
  * (('wait'));-)

= Ganancia $0

  # image
  # src = img/yu.jpeg
  # keep_ratio = true
  # width = 800
  # height = 600 

= Pero...

  * (('wait'))No perdemos tiempo

= Pero...

  * No perdemos tiempo en:
    * Repartir el vuelto
    * (('wait'))Cargar horas
    * (('wait'))Contabilizar horas
    * (('wait'))Contabilizar feriados

= Pero...

  * No perdemos tiempo

= Pero...

  * (('del:No perdemos tiempo'))
  * Ganamos tiempo
  * (('wait'))Y quizás creamos un producto útil para otros

= Resource Map

  * (('wait'))Continous Deployment

= Otros...

  * (('wait'))Entrevista
  * (('wait'))Soporte: GeoChat, Nuntium, Verboice, Chile
  * (('wait'))Ojo
  * (('wait'))Unicef

= Estadísticas

= Estadísticas

  # image
  # src = img/hours_per_keyword.png
  # keep_ratio = true
  # width = 800
  # height = 600 

= Estadísticas

  # image
  # src = img/hours_per_project.png
  # keep_ratio = true
  # width = 800
  # height = 600 

= Estadísticas

  # image
  # src = img/allocation.png
  # keep_ratio = true
  # width = 1024
  # height = 600

= Estadísticas

  # image
  # src = img/fin.png
  # keep_ratio = true
  # width = 1024
  # height = 600